﻿import request from 'request';
import { logger } from '../log';

const iaaiRequestParams = {
    CountOfVehicles: 1,
    Keyword: '',
    Latitude: '',
    Longitude: '',
    Scope: null,
    SortAscending: false,
    SortField: '',
    StartIndex: 1,
    TimeZoneID: 120,
    RequestedApp: 4,
    SelectedRefiners: [{ RefinerTypeValue: 'readyforbid', RefinerValue: true }],
    SortRule: [{ Ascending: true, FieldName: 'liveDatetime' }]
};

export class iaaiRefiners {
    public refiners: any;
    public lastUpdated: Date | null;
    constructor(updateInterval = 60000 * 60 * 12) {
        this.refiners = [];
        this.lastUpdated = null;
        this.update();
        setInterval(this.update.bind(this), updateInterval);
    }

    update() {
        const options = {
            url: 'https://mapp.iaai.com/acserviceswebapi/api/GetSearchResults/',
            json: iaaiRequestParams,
            timeout: 2000
        };

        request.post(options, (error, response, body) => {
            if (!error && response.statusCode === 200 && body) {
                this.refiners = body.Refiners;
                this.lastUpdated = new Date();
                logger.info('Update IAAi refiners');
            } else {
                logger.error('Error while updating iaai refiners ' + error);
                setTimeout(() => {
                    this.update();
                }, 10000);
            }
        });
    }

    checkRefiner(refiner: string, refValue: string) {
        refValue = refValue.toLowerCase();
        for (const ref of this.refiners) {
            if (ref.RefinerTypeValue === refiner) {
                for (const ref1 of ref.Refiners) {
                    if (ref1.RefinerValue.toLowerCase() === refValue) {
                        return true;
                    }
                }
            }
        }
    }
}
