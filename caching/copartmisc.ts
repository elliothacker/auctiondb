﻿import request from 'request';
import { logger } from '../log';
import models from './models.json';
import makes from './makes.json';

const headers = {
    'accept': 'application/json, text/plain, */*',
    'sitecode': 'CPRTUS',
    'company': 'COPART',
    'Content-Type': 'application/json;charset=utf-8',
    'Connection': 'Keep-Alive',
    'User-Agent': 'okhttp/3.4.1'
};


export class CopartMISC {
    CopartMISC: any[];
    makes: string[];
    models: any;
    lastUpdated: Date | null;

    constructor(updatingInterval = 60000 * 60 * 12) {
        logger.debug('Creating Copart Misc updator')
        this.makes = makes;
        this.models = models;
        this.CopartMISC = [];
        this.lastUpdated = null;
        this.updateMISC();
        setInterval(this.updateMISC.bind(this), updatingInterval);
    }
    findModels(make: string) {
        return this.models[make] ? this.models[make] : null;
    }

    checkMake(make: string) {
        return this.checkRefiner('lot_make_desc', make)
    }

    checkRefiner(refiner: string, refValue: string) {
        refValue = refValue.toLowerCase();
        for (const ref of this.CopartMISC) {
            if (ref.RefinerTypeValue === refiner) {
                for (const ref1 of ref.Refiners) {
                    if (ref1.toLowerCase() === refValue) {
                        return ref1;
                    }
                }
            }
        }
    }

    updateMISC() {
        const options = {
            headers,
            url: 'https://mmember.copart.com/srchfilters/',
            form: {
                userStartUtcDatetime: '2019-01-04T16:00:00Z'
            },
            timeout: 2000
        };

        request.post(options, (error, response, body) => {
            if (!error && response.statusCode === 200 && body) {
                body = JSON.parse(body);
                const misc = body.facet_counts.facet_fields;
                const copartMisc: any[] = [];
                for (const mis in misc) {
                    const timedMisc: any[] = [];
                    misc[mis].forEach((m, i) => {
                        if (i % 2 === 0) {
                            timedMisc.push(m);
                        }
                    });
                    copartMisc.push({ RefinerTypeValue: mis, Refiners: timedMisc });
                }
                this.CopartMISC = copartMisc;
                this.lastUpdated = new Date();
                logger.info('Updated Copart MISC');
            } else {
                logger.error('Error while getting misc: ' + error);
            }
        });
    }
}
