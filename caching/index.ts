import { CopartMISC } from './copartmisc';
import { iaaiRefiners } from './iaaiRrefiners';
import { Currency } from './currency';
import commonRefiners from './commonRefiners.json';

export function caching(apiKey: string) {
  return {
    commonRefiners,
    CopartMISC: new CopartMISC(),
    iaaiRefiners: new iaaiRefiners(),
    currency: new Currency(apiKey),
  };
}