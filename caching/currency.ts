﻿import request from 'request';
import { logger } from '../log';


export class Currency {
    private key: string;
    public currencies: any;
    public lastUpdated: Date | null;
    constructor(key: string, updateInterval = 60000 * 60 * 24) {
        this.key = key;
        this.lastUpdated = null;
        this.updateCurrencies();
        setInterval(this.updateCurrencies.bind(this), updateInterval);

    }

    updateCurrencies() {
        request({
            uri: `http://apilayer.net/api/live?access_key=${this.key}`,
            method: 'GET'
        }, (error, response, body) => {
            if (!error && response.statusCode === 200 && body) {
                this.currencies = JSON.parse(body).quotes;
                this.lastUpdated = new Date();
                logger.info('Updated Currency');
            } else {
                logger.error('Updating currencies ' + error);
            }
        });
    }
}





() => {
    var request = require("request");
    function update_currencies() {

    }
    update_currencies();
    setInterval(function () { update_currencies(); }, 60000 * 60);
};


