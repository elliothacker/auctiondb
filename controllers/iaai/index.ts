import express from 'express';
import { iaaiSearch } from './search';
import { iaaiCarInfo } from './carinfo';
import { Params } from '../../types';

export function iaaiRoutes(params: Params) {
  const routes = express.Router();
  routes.get('/search', iaaiSearch(params));
  routes.get('/carinfo', iaaiCarInfo(params));
  routes.get('/refiners', (req, res) => {
    res.json(params.cache.iaaiRefiners.refiners);
  });
  return routes;
}