﻿import request from 'request';
import { Params } from '../../types';
import { logger } from '../../log';

const headers = {
    'Connection': 'Keep-Alive',
    'User-Agent': 'okhttp/2.7.0'
};

export function iaaiCarInfo({ redisClient, config }: Params) {
    return (req, res) => {
        const lotID = parseInt(req.query.id || req.query.lot);
        if (isNaN(lotID) || lotID.toString().length !== 8) {
            res.status(500).json({ error: true, reason: 'Incorrect lotID', reason_id: 2 });
            return;
        }

        redisClient.get(`ic-${lotID}`, (err, reply) => {
            if (!err && reply) {
                res.setHeader('Content-Type', 'application/json');
                res.end(reply);
            } else {
                const options = {
                    headers,
                    url: `https://mapp.iaai.com/VehicleDetails.svc/V2/itemid/${lotID}/`,
                    timeout: config.timeout
                };
                request.get(options, (error, response, body) => {
                    if (!error && response.statusCode === 200 && body) {
                        res.setHeader('Content-Type', 'application/json');
                        res.end(body);
                        redisClient.set(`ic-${lotID}`, body, 'EX', 24 * 3600);
                    } else {
                        res.status(500).json({ error: true, reason: 'Iaai server error', reason_id: 1 });
                        logger.error('Iaai carinfo error ' + error);
                    }
                });
            }
        });
    }
};