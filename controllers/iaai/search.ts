﻿import request from 'request';
import { Params } from '../../types';
import { logger } from '../../log';

function escape(param) {
    if (param) {
        return encodeURIComponent(param);
    }
    return '';
}
const ignoreParams = [
    'yearstart', 'year_start', 'yearend', 'year_end', 'cache', 'page', 'count'
]
export function iaaiSearch({ config, redisClient, cache }: Params) {
    return (req, res) => {
        const ret: any = {
            CountOfVehicles: 20,
            Keyword: "",
            Scope: "",
            SortAscending: (req.query.sort_type === 'asc'),
            SortField: (!req.query.sort ? 'liveDatetime' : req.query.sort),
            StartIndex: 1,
            TimeZoneID: 120,
            RequestedApp: 3,
            SortRule: [
                {
                    Ascending: (req.query.sort_type === 'asc'),
                    FieldName: (!req.query.sort ? 'liveDatetime' : req.query.sort)
                }
            ],
            SelectedRefiners: []
        };
        const searchRefiners: any[] = [
            {
                RefinerTypeValue: 'readyforbid',
                RefinerValue: true
            }
        ];
        let countOfRefiners = 0;
        for (const ref in req.query) {
            const refValue = req.query[ref];
            if (!refValue || ignoreParams.includes(ref)) {
                continue;
            }
            if (cache.iaaiRefiners.checkRefiner(ref, refValue)) {
                searchRefiners.push({
                    RefinerTypeValue: ref,
                    RefinerValue: refValue
                });
            } else {
                res.status(404).json({ success: false, error: `Refiner ${ref} with value ${refValue} doesn't exist` });
                return;
            }
            countOfRefiners++;
            if (countOfRefiners > 19) {
                return false;
            }
        }
        ret.SelectedRefiners = searchRefiners;
        const yearStart = escape(req.query.year_start || req.query.yearstart);
        const yearEnd = escape(req.query.year_end || req.query.yearend);
        if (yearStart || yearEnd) {
            if (yearEnd && yearStart) {
                ret.SelectedRefiners.push({
                    RefinerTypeValue: 'yearfilter',
                    RefinerValue: `${yearStart}-${yearEnd}`
                });
            } else {
                if (yearStart) {
                    ret.SelectedRefiners.push({
                        RefinerTypeValue: 'yearfilter',
                        RefinerValue: `${yearStart}-2020`
                    });
                } else {
                    ret.SelectedRefiners.push({
                        RefinerTypeValue: 'yearfilter',
                        RefinerValue: `2016-${yearEnd}`
                    });
                }
            }
        }
        let count = req.query.count;
        if (isNaN(count)) {
            count = 20;
        }
        ret.CountOfVehicles = count;
        let page = req.query.page;
        if (isNaN(page)) {
            page = 1;
        }
        ret.StartIndex = (page - 1) * count + 1;

        const key = `is-${count}-${page}-${JSON.stringify(ret)}`;
        redisClient.get(key, (err, reply) => {
            if (!err && reply) {
                res.setHeader('Content-Type', 'application/json');
                res.end(reply);
            } else {
                const options = {
                    url: 'https://mapp.iaai.com/acserviceswebapi/api/GetSearchResults/',
                    json: ret,
                    timeout: config.timeout
                };

                request.post(options, (error, response, body) => {
                    if (!error && response.statusCode === 200 && body) {
                        delete body.Refiners;

                        body.Vehicles.forEach((veh, i) => {
                            for (const key in veh) {
                                if (!veh[key]) {
                                    delete body.Vehicles[i][key];
                                }
                            }
                        });
                        res.json(body);
                        redisClient.set(key, JSON.stringify(body), 'EX', 5 * 3600);
                    } else {
                        res.status(500).json({ error: true, reason: 'Iaai server error', reason_id: 1 });
                        logger.error('While iaai search ' + error);
                    }
                });
            }
        });
    }
};