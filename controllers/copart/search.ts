﻿import request from 'request';
import { Params } from '../../types';
import { logger } from '../../log';

const headers = {
    'accept': 'application/json, text/plain, */*',
    'sitecode': 'CPRTUS',
    'company': 'COPART',
    'Content-Type': 'application/json;charset=utf-8',
    'Connection': 'Keep-Alive',
    'User-Agent': 'okhttp/3.4.1'
};

function escape(param: string): string {
    if (param !== undefined && typeof param === 'string') {
        return encodeURIComponent(param);
    }
    return '';
}


export function copartSearch({ redisClient, config, cache }: Params) {
    return (req, res) => {
        const make = escape(req.query.make);
        const model = escape(req.query.model);
        const yearStart = escape(req.query.year_start || req.query.yearstart);
        const yearEnd = escape(req.query.year_end || req.query.yearend);
        const odometerStart = escape(req.query.odometer_start);
        const odometerEnd = escape(req.query.odometer_end);
        const sort = escape(req.query.sort);
        const sortType = escape(req.query.sort_type);

        const misc = ["vehicle_type_code:VEHTYPE_V"];

        if (make && make !== '*') {
            const checkedMake = cache.CopartMISC.checkMake(make);
            if (checkedMake) {
                misc.push(`lot_make_desc: "${checkedMake}"`);
                if (model && model !== '*') {
                    const checkedModel = model.toUpperCase();
                    misc.push(`lot_model_group: "${checkedModel}" `);
                    // OR lot_model_desc: "${model}"
                }
            }
        }
        if (yearStart || yearEnd) {
            if (yearStart && yearEnd) {
                misc.push(`lot_year:["${yearStart}" TO "${yearEnd}"]`);
            } else {
                if (yearStart) {
                    misc.push(`lot_year:["${yearStart}" TO "2020"]`);
                } else {
                    misc.push(`lot_year:["2016" TO "${yearEnd}"]`);
                }
            }
        }

        if (odometerStart || odometerEnd) {
            if (odometerStart && odometerEnd) {
                misc.push(`odometer_reading_received:["${odometerStart}" TO "${odometerEnd}"]`);
            } else {
                if (odometerStart) {
                    misc.push(`odometer_reading_received:["${odometerStart}" TO "99999"]`);
                } else {
                    misc.push(`odometer_reading_received:["0" TO "${odometerEnd}"]`);
                }
            }
        }
        let count = req.query.count;
        if (isNaN(count)) {
            count = 20;
        }

        const page = parseInt(req.query.page) || 1;
        let sorting = ['auction_date_type desc'];

        if (sort) {
            sorting = [sort + (sortType === 'asc' ? ' asc' : ' desc')];
        }

        let countOfRefiners = 0;
        for (const ref in req.query) {
            if (cache.CopartMISC.checkRefiner(ref, req.query[ref])) {
                misc.push(ref + ":\"" + req.query[ref] + '"');
            }
            countOfRefiners += 1;
            if (countOfRefiners > 19) {
                break;
            }
        }

        const key = `cs-${count}-page-${sorting}-${JSON.stringify(misc)}`;
        redisClient.get(key, function (err, reply) {
            if (!err && reply) {
                res.setHeader('Content-Type', 'application/json');
                res.end(reply);
            } else {
                const options = {
                    headers,
                    url: 'https://mmember.copart.com/srch/',
                    form: {

                        MISC:
                            misc,
                        latlng: "0",
                        sort: sorting,
                        pageNumber: page,
                        userStartUtcDatetime: "2018-07-30T07:00:00Z",
                        latlngFacets: true
                    },
                    timeout: config.timeout
                };

                request.post(options, (error, response, body) => {
                    if (!error && response.statusCode === 200 && body) {
                        try {
                            body = JSON.parse(body);
                        } catch (err) {
                            res.status(500).json({ error: true, reason: 'Copart server error', reason_id: 1 });
                            logger.error(err);
                            return;
                        }
                        if (body) {
                            res.json(body['response']);
                            redisClient.set(key, JSON.stringify(body['response']), 'EX', 5 * 3600);

                        } else {
                            res.status(500).json({ error: true, reason: 'Copart server error', reason_id: 1 });
                        }
                    } else {
                        res.status(500).json({ error: true, reason: 'Copart server error', reason_id: 1 });
                        logger.error('Copart search error ' + error);
                    }
                });
            }
        });
    }
};