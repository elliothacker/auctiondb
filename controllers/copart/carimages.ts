﻿import request from 'request';
import { Params } from '../../types';
import { logger } from '../../log';

const headers = {
    'accept': 'application/json, text/plain, */*',
    'sitecode': 'CPRTUS',
    'company': 'COPART',
    'Content-Type': 'application/json;charset=utf-8',
    'Connection': 'Keep-Alive',
    'User-Agent': 'okhttp/3.4.1'
};

export function copartCarImages({ config, redisClient }: Params) {
    return (req, res) => {
        const lotID = parseInt(req.query.id || req.query.lot);
        if (isNaN(lotID) || lotID.toString().length !== 8) {
            res.status(500).json({ error: true, reason: 'Incorrect lotID', reason_id: 2 });
            return;
        }

        redisClient.get(`cci-${lotID}`, (err, reply) => {
            if (!err && reply) {
                res.setHeader('Content-Type', 'application/json');
                res.end(`{"docs": ${reply}}`);
            } else {
                const options = {
                    headers,
                    url: 'https://mmember.copart.com/solrlotimages/',
                    form: {
                        lotNumber: lotID
                    },
                    timeout: config.timeout
                };

                request.post(options, (error, response, body) => {
                    if (!error && body && response.statusCode === 200) {
                        body = JSON.parse(body);
                        if (body) {
                            let ret: any[] = [];
                            for (const image of body['response']['docs']) {
                                if (image['jpg_type'] === 'F') {
                                    ret.push({ url: image['url'] });
                                }
                            }
                            res.json({ docs: ret });
                            redisClient.set(`cci-${lotID}`, JSON.stringify(ret), 'EX', 24 * 3600);

                        } else {
                            res.status(500).json({ error: true, reason: 'Copart server error', reason_id: 1 });
                        }
                    } else {
                        res.status(500).json({ error: true, reason: 'Copart server error', reason_id: 1 });
                        logger.error('Copart images ' + error);
                    }
                });
            }
        });
    }
}