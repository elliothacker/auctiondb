import express from 'express';
import { copartSearch } from './search';
import { copartCarInfo } from './carinfo';
import { copartCarImages } from './carimages';
import { Params } from '../../types';

export function copartRoutes(params: Params) {
  const routes = express.Router();
  routes.get('/search', copartSearch(params));
  routes.get('/carinfo', copartCarInfo(params));
  routes.get('/images', copartCarImages(params));
  routes.get('/refiners', (req, res) => {
    res.json(params.cache.CopartMISC.CopartMISC);
  });
  return routes;
}