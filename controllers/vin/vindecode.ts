﻿import vindec from 'vindec';
import request from 'request';

export function decodeVIN(vin: string): Promise<any> {
    return new Promise((resolve, reject) => {
        if (vindec.validate(vin)) {
            request({
                uri: "https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVINValuesBatch/",
                method: "POST",
                form: {
                    DATA: vin,
                    format: 'json'
                },
                timeout: 1500
            }, (error, response, body) => {
                if (!error && response.statusCode === 200) {
                    resolve(JSON.parse(body));
                    return;
                }
                reject(error);
            });
        } else {
            reject(new Error('vin error'));
        }
    });
};