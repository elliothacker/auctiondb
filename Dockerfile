FROM node:lts-alpine

ENV NODE_ENV=production

RUN apk add --update npm

COPY . /app
WORKDIR /app

RUN npm install --production
RUN npm install typescript -g 
RUN npm run build
RUN npm remove typescript -g 


EXPOSE 8080

CMD ["npm", "start"]