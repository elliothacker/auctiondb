﻿import express from 'express';
import { Params } from '../types';
import vindec from 'vindec';
import { decodeVIN } from '../controllers/vin/vindecode';
import { iaaiRoutes } from '../controllers/iaai';
import { copartRoutes } from '../controllers/copart';
import { status } from './status';


export function createRoutes(params: Params) {
    const { cache, config } = params;
    const routes = express.Router();

    routes.get('/', (req, res) => {
        res.send('This is auctiondb base api URL. Before using read the manual.');
    });

    routes.get('/status', status(params))

    routes.use((req, res, next) => {
        if (!config.auth || (req.query.key && config.keys.includes(req.query.key))) {
            delete req.query.key;
            next();
        } else {
            res.status(401).json({ success: false, message: 'API Key is invalid or has expired' });
        }
    })

    routes.get('/currency', (req, res) => {
        res.json(cache.currency.currencies);
    });

    routes.get(['/make', '/makes'], (req, res) => {
        res.json({ success: true, make: cache.CopartMISC.makes });
    });

    routes.get(['/model', '/models'], (req, res) => {
        if (req.query.make) {
            const make = req.query.make.toLowerCase();
            const models = cache.CopartMISC.findModels(make);
            res.json({ success: !!models, models: models ? models : [] });
        } else {
            res.json({ success: false, models: [] });
        }
    });

    routes.get('/vin/decode', (req, res) => {
        let vin = req.query.vin;
        if (vin) {
            vin = vin.trim();
            vindec(vin, (result: any) => {
                if (result) {
                    result = {
                        valid: true,
                        error: false,
                        success: true,
                        ...result
                    }
                    res.json(result);
                } else {
                    res.json({ valid: true, error: true, success: true });
                }
            });
        } else {
            res.json({ valid: false });
        }
    });

    routes.get('/vin/search', (req, res) => {
        if (req.body.vin) {
            decodeVIN(req.body.vin).then((decoded) => {
                res.json(decoded)
            }).catch((err) => {
                res.status(500).json({ error: 'Error while decoding vin' });
            })
        }
    });

    routes.use('/copart', copartRoutes(params));
    routes.use('/iaai', iaaiRoutes(params));
    routes.get('/refiners', (req, res) => { res.json(cache.commonRefiners); });

    routes.use((req, res, next) => {
        res.status(404).json({ success: false, error: 'Method is not found' })
        next();
    })

    return routes;
}
