import { Params } from '../types';

export function status({ cache, redisClient }: Params) {
  return (req, res) => {
    res.json({
      success: true,
      status: cache.currency.lastUpdated && cache.iaaiRefiners.lastUpdated && cache.CopartMISC.lastUpdated ? true : false,
      lastUpdated: {
        currency: cache.currency.lastUpdated,
        iaaiRefiners: cache.iaaiRefiners.lastUpdated,
        copartRefiners: cache.CopartMISC.lastUpdated
      },
      redisStatus: redisClient.connected
    });
  };
}