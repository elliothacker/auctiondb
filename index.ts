﻿import config from './config.json';
import redis from 'redis';
import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import swStats from 'swagger-stats';
import { logger } from './log';
import { createRoutes } from './routes';
import { caching } from './caching';

logger.info('Starting api server')

const redisClient = redis.createClient({
    host: 'redis',
    port: 6379
});
redisClient.on('error', (err) => {
    logger.error(err.toString());
});

const app = express();
app.use(swStats.getMiddleware({
    uriPath: '/monitor',
    authentication: true,
    onAuthenticate(req, username, password) {
        return username === config.username && password === config.password
    }
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors({
    // origin: ['https://americars.com.ua', 'https://brok.kiev.ua/'],
    maxAge: 86400
}));

const cache = caching(config.currencyApi);


app.use('/', createRoutes({ cache, redisClient, config }));
logger.info('Created routes');
app.listen(config.port);