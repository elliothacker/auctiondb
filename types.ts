import { RedisClient } from 'redis';
import { CopartMISC } from './caching/copartmisc';
import { iaaiRefiners } from './caching/iaaiRrefiners';
import { Currency } from './caching/currency';

export interface Params {
  config: any;
  cache: {
    CopartMISC: CopartMISC;
    iaaiRefiners: iaaiRefiners;
    currency: Currency;
    commonRefiners: any;
  };
  redisClient: RedisClient;
}


